%include "util.asm"

section .data
    qtxt db "Enter your name: ", 0h
    wtxt db "Hello, ", 0h

section .bss
    sinp: resb 255

section .text
    global _start

_start:
    mov     rsi, qtxt
    call   _print 

    mov     rax, 0
    mov     rdi, 0
    mov     rsi, sinp
    mov     rdx, 255
    syscall

    mov     rsi, wtxt
    call    _print
    mov     rsi, sinp
    call    _print

    call    _quit
