section .data
    txt db "TEXT 2 ABC", 0Ah

section .text
    global _start

_start:
    mov     rax, txt
    mov     rdx, rax

_next:
    cmp     byte [rdx], 0
    jz      _end
    inc     rdx
    jmp     _next

_end:
    sub     rdx, rax

    mov     rax, 1
    mov     rdi, 1
    mov     rsi, txt
    syscall

    mov     rax, 60
    mov     rdi, 0
    syscall
