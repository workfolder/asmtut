;============== print ==============
_print:
    push    rax
    push    rdi
    push    rdx
    
    mov     rax, 1
    mov     rdi, 1
    call    _len
    syscall
    
    mov     rsi, 0Ah
    push    rsi
    
    mov     rax, 1
    mov     rdi, 1
    mov     rsi, rsp
    mov     rdx, 1
    syscall
    
    pop     rsi
  
    pop     rdx
    pop     rdi
    pop     rax
    
    ret

;============== len ==============
_len:
    mov     rdx, rsi

_next_:
    cmp     byte [rdx], 0
    jz      _end_
    inc     rdx
    jmp     _next_

_end_:
    sub     rdx, rsi

    ret

;============== quit ==============
_quit:
    mov     rax, 60
    mov     rdi, 0
    syscall
    
    ret
