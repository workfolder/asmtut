%include "util.asm"

section .text
    global _start

_start:
    mov     rax, 0

_loop_:
    mov     rsi, rax
    add     rsi, 48
    push    rsi
    mov     rsi, rsp
    call    _print
    pop     rsi

    inc     rax
    cmp     rax, 10
    jne     _loop_

    call    _quit
