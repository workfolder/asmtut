section .data
    txt db "TEXT"

section .text
    global _start

_start:
    mov     rax, 1
    mov     rdi, 1
    mov     rsi, txt
    mov     rdx, 4
    syscall

    mov     rax, 60
    mov     rdi, 0
    syscall
