;======== print string ========
__print:
    push    rax
    push    rdi
    push    rdx

    mov     rax, 1
    mov     rdi, 1
    call    __len
    syscall
    
    pop     rdx
    pop     rdi
    pop     rax
    ret

;======== get string length ========
__len:
    mov     rdx, rsi

_next:
    cmp     byte [rdx], 0
    jz      _end
    inc     rdx
    jmp     _next

_end:
    sub     rdx, rsi
    ret

;======== exit program ========
__quit:
    mov     rax, 60
    mov     rdi, 0
    syscall
    ret
