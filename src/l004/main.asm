section .data
    txt db "SAMPLE TEXT", 0Ah

section .text
    global _start

_start:
    mov     rax, 1
    mov     rdi, 1
    mov     rsi, txt
    call    __len
    syscall

    mov     rax, 60
    mov     rdi, 0
    syscall

__len:
    push    rax
    
    mov     rax, txt

_next:
    cmp     byte [rax], 0
    jz      _end
    inc     rax
    jmp     _next

_end:
    sub     rax, rsi    
    mov     rdx, rax

    pop     rax

    ret
