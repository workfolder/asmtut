%include "util.asm"

section .data
    txt1 db "TEXT 1", 0Ah, 0h
    txt2 db "TEXT 2", 0Ah, 0h

section .text
    global _start

_start:
    mov     rsi, txt1
    call    __print

    mov     rsi, txt2
    call    __print

    call    __quit


