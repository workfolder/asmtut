;============= printn =============
_printn:
    call    _print
    mov     rsi, 0Ah
    push    rsi
    mov     rsi, rsp
    call    _print
    pop     rsi
    ret

;============= print =============
_print:
    push    rax
    push    rdi
    push    rdx
    mov     rax, 1
    mov     rdi, 1
    call    _len
    syscall
    pop     rdx
    pop     rdi
    pop     rax
    ret

;============= len =============
_len:
    mov     rdx, rsi
__next:
    cmp     byte [rdx], 0
    jz      __end
    inc     rdx
    jmp     __next
__end:
    sub     rdx, rsi
    ret

;============= quit =============
_quit:
    mov     rax, 60
    mov     rdi, 0
    syscall
    ret
