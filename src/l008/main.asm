%include "util.asm"

section .text
    global _start

_start:
    pop     rax

__narg:
    cmp     rax, 0
    jz      __qarg
    pop     rsi
    call    _printn
    dec     rax
    jmp     __narg

__qarg:
    call    _quit
