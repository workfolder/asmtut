%include "util.asm"

section .data
    txt1 db "TEXT 1", 0h
    txt2 db "TEXT 2", 0h

section .text
    global _start

_start:
    mov     rsi, txt1
    call    _printn

    mov     rsi, txt2
    call    _print

    call    _exit
